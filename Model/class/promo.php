<?php

/**
 * classe Promo
 */
class Promo
{
	protected $id;
	protected $dateDeb;
	protected $dateFin;
	protected $lieu;
	protected $loginInter;
	protected $heureTypeDeb;
	protected $heureTypeFin;

	function __construct(
		$id = null, $dateDeb = null, $dateFin = null, $lieu = null, $loginInter = null, $heureTypeDeb = null, $heureTypeFin = null)
	{
		if($id == null)
		{
			$this->dateDeb      = $dateDeb;
			$this->dateFin      = $dateFin;
			$this->lieu         = $lieu;
			$this->loginInter   = $loginInter;
			$this->heureTypeDeb = $heureTypeDeb;
			$this->heureTypeFin = $heureTypeFin;
		}
		else
		{
			$this->id = $id;
			$this->load();
		}
	}

  public function getId(){return $this->id;}
  public function getDateDeb(){return $this->dateDeb;}
  public function getDateFin(){return $this->dateFin;}
  public function getLieu(){return $this->lieu;}
  public function getLoginInter(){return $this->loginInter;}
  public function getHeureTypeDeb(){return $this->heureTypeDeb;}
  public function getHeureTypeFin(){return $this->heureTypeFin;}

  public function setDateDeb($dateDeb) {$this->dateDeb=$dateDeb;}
  public function setDateFin($dateFin) {$this->dateFin=$dateFin;}
  public function setLieu($lieu){ $this->lieu=$lieu;}
  public function setLoginInter($loginInter){ $this->loginInter=$loginInter;}
  public function setHeureTypeDeb($heureDeb){ $this->heureTypeDeb=$heureDeb;}
  public function setHeureTypeFin($heureFin){ $this->heureTypeFin=$heureFin;}

	private function load()
	{
		require('bdd.php');
		$requete = $db->prepare("SELECT * FROM Promo WHERE id = ?");
		$requete->bindParam(1, $this->id);

		if($requete->execute()==false)
		{
			die('Il y a eu un problème lors de la récupération des informations');
		}
		else
		{
			$infos = $requete->fetch(PDO::FETCH_ASSOC);

			$this->dateDeb      = $infos['dateDeb'];
			$this->dateFin      = $infos['dateFin'];
			$this->lieu         = $infos['lieu'];
			$this->loginInter   = $infos['loginInter'];
			$this->heureTypeDeb = $infos['heureTypeDeb'];
			$this->heureTypeFin = $infos['heureTypeFin'];
		}
	}

	public function save()
	{
		require('bdd.php');
		$requete = $db->prepare('INSERT INTO Promo (dateDeb,dateFin,lieu,loginInter,heureTypeDeb,heureTypeFin) values(?,?,?,?,?,?)');

		$requete->bindParam(1,$this->dateDeb);
		$requete->bindParam(2,$this->dateFin);
		$requete->bindParam(3,$this->lieu);
		$requete->bindParam(4,$this->loginInter);
		$requete->bindParam(5,$this->heureTypeDeb);
		$requete->bindParam(6,$this->heureTypeFin);

		if($requete->execute()==false){
			echo 't\'es nulle 1';
			die;
		}
		else{
			$this->id=$db->lastInsertId();
		}
	}

  public function update()
  {
		require('bdd.php');
		$requete =
		'UPDATE Promo
		SET dateDeb  = ?,
		dateFin      = ?,
		lieu         = ?,
		loginInter   = ?,
		heureTypeDeb = ?,
		heureTypeFin = ?
		WHERE id     = ?';
		$requete = $db->prepare($requete);
		$requete->bindParam(1, $this->dateDeb);
		$requete->bindParam(2, $this->dateFin);
		$requete->bindParam(3, $this->lieu);
		$requete->bindParam(4, $this->loginInter);
		$requete->bindParam(5, $this->heureTypeDeb);
		$requete->bindParam(6, $this->heureTypeFin);
		$requete->bindParam(7, $this->id, PDO::PARAM_INT);//protection en +
		if($requete->execute()==false){
			die('erreur :'.$requete);
		}
	}


  public function delete()
	{
		require('bdd.php');
		$requete = $db->prepare(
			"DELETE FROM Promo WHERE id = ?"
		);
		$requete->bindParam(1, $this->id);
		if($requete->execute()==false)
		{
			die('Il y a eu un problème lors de la suppression');
		}
	}

	static function getList(){
		require('bdd.php');
		$liste = array();

		$requete = 'SELECT id FROM Promo ORDER BY id DESC';
		$req = $db->prepare($requete);
		if($req->execute()==false){
			die('erreur : impossible de récupérer la liste');
		}
		else{
			
			while($inf = $req->fetch(PDO::FETCH_ASSOC)){
				$liste[] = new Promo($inf['id']);
			}
		}

		return $liste;
	}

}


?>
