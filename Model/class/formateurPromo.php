<?php
/**
 * Classe FormateurPromo
 */

class FormateurPromo{
    protected $id;
    protected $idFormateur;
    protected $idPromo;

        function __construct($id=null, $idFormateur=null, $idPromo=null)
        {
            if($id == null)
            {
                $this->idFormateur      = $idFormateur;
                $this->idPromo          = $idPromo;
            }
            else
            {
                $this->id = $id;
                $this->load();
            } 
        }

    public function getId(){return $this->id;}
    public function getidFormateur(){return $this->idFormateur;}
    public function getidPromo(){return $this->idPromo;}
         
    public function setidFormateur($idFormateur) {$this->idFormateur=$idFormateur;}
    public function setidPromo($idPromo) {$this->idPromo=$idPromo;}
    
        private function load()
        {
            require('bdd.php');
            $requete = $db->prepare("SELECT * FROM FormateurPromo WHERE id = ?");
            $requete->bindParam(1, $this->id);

            if($requete->execute()==false)
            {
            die('Il y a eu un problème lors de la récupération des informations');
            }
        else
            {
                $infos = $requete->fetch(PDO::FETCH_ASSOC);

                $this->idFormateur     = $infos['idFormateur'];
                $this->idPromo  = $infos['idPromo'];
            
            
            }
        }

        public function save()
	    {
            require('bdd.php');
            $requete = $db->prepare('INSERT INTO FormateurPromo (id, idFormateur, idPromo) values(?,?,?)');

            $requete->bindParam(1,$this->id);
            $requete->bindParam(2,$this->idFormateur);
            $requete->bindParam(3,$this->idPromo);
            

		    if($requete->execute()==false){
                echo 't\'es nulle 1';
                die;
		    }
		    else{
			    $this->id=$db->lastInsertId();
		    }
	    }


}

?>