class LoginAdmin {
	constructor() {
		
		this.login = document.getElementById('loginAdmin');
		this.mdp = document.getElementById('mdpAdmin');		
		
		this.login.addEventListener('blur', () => {
			this.verify(this.login);
		});
		this.mdp.addEventListener('blur', () => {
			this.verify(this.mdp);
		});
	}

	setLogin(login){ this.login = login; }
	setMdp(mdp){ this.mdp = mdp; }

	verify(donnee=null){
		if(donnee !== null)
		{
			if(donnee.value.length < 1)
			{
				this.surligne(donnee, true);
				return false;
			}
			else
			{
				this.surligne(donnee, false);
				return true;
			}
		}
	}

	surligne(champ, erreur)
	{
		if(erreur)
		{
			champ.style.backgroundColor = "#fba";
		}
		else
		{
			champ.style.backgroundColor = "";
		}
	}

  

}

let loginAdmin = new LoginAdmin(); 

