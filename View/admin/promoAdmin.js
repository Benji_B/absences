class PromoAdmin{

  constructor(){
    $("#ville").autocomplete({
      source: function (request, response) {
        $.ajax({
          url: "https://vicopo.selfbuild.fr/ville/"+request.term,
          dataType: "json",
          success: function (data) {
            response($.map(data.cities, function (item) {
              return { label: item.city,
                       value: item.city
              };
            }));
          }
        });
      },
    });
    $("#dateDeb").datepicker({
      dateFormat: "dd-mm-yy",     
      onSelect: function (date) {
          let date2 = $('#dateDeb').datepicker('getDate');
          // date2.setDate(date2.getDate() + 1);
          // $('#dateFin').datepicker('setDate', date2);
          //sets minDate to dateDeb date + 1   
          $('#dateFin').datepicker({
            dateFormat: "dd-mm-yy",
            minDate:$('#dateDeb').datepicker('getDate'),
            onClose: function () {
              let dateDeb = $('#dateDeb').datepicker('getDate');
              console.log(dateDeb);
              let dateFin = $('#dateFin').datepicker('getDate');
              if (dateFin <= dateDeb) {
                $('#dateFin').datepicker('setDate', dateDeb);
              }
            }
          });       
      }
    });
    
    
    this.equipe = $('#equipe');
    this.add = $('#add');
    this.compteur = 0;

    this.add.click(() => {
      this.addField();
      
      this.compteur++;
      if (this.compteur >= 1) {
        this.remove = $('.remove');
        console.log(this.remove);
        console.log(this.add);
           
        this.remove.click( function() {
          this.closest(".equipe_input").remove();
          this.compteur--;
        });
      }
    });
    
  }

  addField(){
		this.equipe.append('<div class="equipe_input">'+
				'<div class="equipe_nom">'+
					'<label for="nom">Nom</label>'+
					'<input class="identite" type="text" name="nom[]" value="" required/>'+
				'</div>'+
				'<div class="equipe_prenom">'+
					'<label for="prenom">Prénom</label>'+
					'<input class="identite" type="text" name="prenom[]" value="" required/>'+
				'</div>'+
				'<div class="equipe_login">'+
					'<label for="login">Identifiant</label>'+
					'<input class="login" type="text" name="login[]" value="" required/>'+
				'</div>'+
        '<button class="remove" type="button">-</button>'+
			'</div>');
  }

	// fadeIn(el, time) {
	// 	el.style.opacity = 0;
	// 	let last = +new Date();
	// 	var tick = function() {
	// 		el.style.opacity = +el.style.opacity + (new Date() - last) / time;
	// 		last = +new Date();
	// 		if (+el.style.opacity < 1)
	// 		{
	// 			(window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
	// 		}
	// 	};
	// 	tick();
	// }

	// fadeOut(el, time) {
	// 	el.style.opacity = 1;
	// 	var last = +new Date();
	// 	var tick = function() {
	// 		el.style.opacity = +el.style.opacity - (new Date() - last) / time;
	// 		last = +new Date();
	// 		if (+el.style.opacity > 0)
	// 		{
	// 			(window.requestAnimationFrame && requestAnimationFrame(tick)) || setTimeout(tick, 16);
	// 		}
	// 	};
	// 	tick();
	// }

}

let promoAdmin = new PromoAdmin();

 
