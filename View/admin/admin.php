<?php

/**
 *
 */
class ViewAdmin
{

	function __construct() {

	}

	public function displayFormLogin() {
		echo '
			<div id="formAdmin">
				<img id="logo" src="../Images/logo.png" alt="Logo de Simplon">
				<div id="titreAdmin">
					<h1>ADMINISTRATEUR</h1>
				</div>
				<form class="form_admin" action="" method="post">
					<div class="divLogin">
						<label for="login">Identifiant</label>
						<input id="loginAdmin" type="text" name="login" value="" required>
					</div>
					<div class="divMdp">
						<label for="mdp">Mot de passe</label>
						<input id="mdpAdmin" type="password" name="mdp" value="" required>
					</div>
					<button id="bouton_connexion" type="submit" name="choix" value="connectAdmin">Valider</button>
				</form>
			</div>
			<script type="text/javascript" src="../View/admin/loginAdmin.js"></script>
		';
	}

	public function displayFormPromo() {
		echo '
			<header>
				<div class="logo_header">
					<a href="../Controller/adminControl.php?choix=liste" target="_self">
						<img class="logoHeader" src="../Images/logo.png" alt="Logo de Simplon">
					</a>
				</div>
				<div id="titreAjoutPromo">
					<h1>Ajouter une promotion</h1>
				</div>
			</header>
			<div id="formPromo">
				<form id="form_promo" action="" method="post">
					<div id="dateHeureLieuInter">
						<div id="dates">
							<div id="date_deb">
								<label for="date_deb">Date de début</label>
								<input type="text" id="dateDeb" name="dateDeb" value="" readonly>
							</div>
							<div id="date_fin">
								<label for="date_fin">Date de fin</label>
								<input type="text" id="dateFin" name="dateFin" value="" readonly>
							</div>
						</div>
						<div id="heures">
							<div id="heure_deb">
								<label for="heureTypeDeb">Heure de début de journée</label>
								<input type="time" name="heureTypeDeb" value="" required>
							</div>
							<div id="heure_fin">
								<label for="heureTypeFin">Heure de fin de journée</label>
								<input type="time" name="heureTypeFin" value="" required>
							</div>
						</div>
						<div id="lieuInter">
							<div id="liste_lieu">
								<label for="lieu">Lieu</label>
								<input id="ville" name="lieu" placeholder="Entrez une ville" required/>
							</div>
							<div id="intervenant">
								<label for="loginInter">Identifiant unique des intervenants</label>
								<input id="identiteInter" type="text" name="loginInter" value="" required/>
							</div>
						</div>
					</div>
					<label for="equipe"><h2>Équipe pédagogique</h2></label>
					<div id="equipe">
						<div class="equipe_input">
							<div class="equipe_nom">
								<label for="nom">Nom</label>
								<input class="identite" type="text" name="nom[]" value="" required/>
							</div>
							<div class="equipe_prenom">
								<label for="prenom">Prénom</label>
								<input class="identite" type="text" name="prenom[]" value="" required/>
							</div>
							<div class="equipe_login">
								<label for="login">Identifiant</label>
								<input class="login" type="text" name="login[]" value="" required/>
							</div>              
						</div>
					</div>
					<div id="boutons">
						<button  id="add" type="button">+</button>
						
					</div>
					<button id="valider" type="submit" name="choix" value="addPromo" onclick="return confirm(\'Êtes-vous sûr de vos informations\');">Valider</button>
				</form>
			</div>
			<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
			<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
			<link rel="stylesheet" href="https://code.jquery.com/ui/1.7.3/themes/base/jquery-ui.css">
      <script src="js/jquery-ui.datepicker-fr.js"></script>
			<script type="text/javascript" src="../View/admin/promoAdmin.js"></script>';
	}

	public function displayList($liste=null) {
		echo '
		<header>
			<div class="logo_header">
				<a href="http://localhost/absences/Controller/adminControl.php?choix=liste" target="_self">
					<img class="logoHeader" src="../Images/logo.png" alt="Logo de Simplon">
				</a>
			</div>
			<div id="titreListePromo">
				<h1>Liste des promos</h1>
			</div>
		</header>
		<div id="listePromo">
			<div id="liste_promo">
				<form action="adminControl.php">
				<button id="addPromo" type="submit" name="choix" value="formulaireAjout">+</button>
				</form>';
					if ($liste!=null) {
				echo'
				<table id="tablePromo">
					<thead id="theadPromo">
						<tr>
							<th>Date de début</th>
              <th>Date de fin</th>
							<th>Ville</th>
						</tr>
					</thead>
					<tbody id="bodyPromo">';

						foreach ($liste as $promo) {
							echo'
              <!-- <a href=""></a> -->
							<tr>
								<td>'.$promo->getDateDeb().'</td>
                <td>'.$promo->getDateFin().'</td>                
								<td>'.$promo->getLieu().'</td>
							</tr>
						';
						}
					}
			echo'
					</tbody>
				</table>
			</div>
		</div>
		';
	}
}

?>