<?php
session_start();
?>
<!DOCTYPE html>
<html lang="fr">
<head>
  <title>Absence Simplon</title>
  <meta charset="utf-8">
  <!-- <link rel="icon" type="image/png" href="../Images/logo.png" /> -->
  <link rel="stylesheet" type="text/css" href="../View/admin/admin.css">
  <link href="https://fonts.googleapis.com/css?family=Asap|Overpass&display=swap" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=Indie+Flower%7CLobster+Two&display=swap" rel="stylesheet">  -->
</head>
<body>
<?php

require ('../View/admin/admin.php');
require ('../Model/class/admin.php');
$viewAdmin= new ViewAdmin();

$choix='connexion';

if (isset($_REQUEST['choix'])){
  $choix = $_REQUEST['choix'];
}

switch ($choix)
{
  case 'connexion':$viewAdmin->displayFormLogin();
  break;

	case 'connectAdmin': //ok
		if(isset($_POST['login'])&& isset($_POST['mdp'])){
			require_once ('../Model/class/admin.php');
      require_once ('../Model/class/promo.php');
			$admin = Admin::adminExist($_POST['login'], ($_POST['mdp']));
			if($admin != null){
				$_SESSION['admin'] = true;
				$liste = Promo::getList();
      $viewAdmin->displayList($liste);
			}
			else{
				echo 'Mauvais login ou mot de passe';
				$viewAdmin->displayFormLogin();
			}
		}
		else {
			$viewAdmin->displayFormLogin();
		}
	break;

	case 'liste':
		if (isset($_SESSION['admin']) && $_SESSION['admin']===true){
			require_once ('../Model/class/promo.php');
			$liste = Promo::getList();
			$viewAdmin->displayList($liste);
		}
		else {
			$viewAdmin->displayFormLogin();
		}
	break;


	case 'formulaireAjout':
		if (isset($_SESSION['admin']) && $_SESSION['admin']===true){
			$viewAdmin->displayFormPromo();
		}
		else {
			$viewAdmin->displayFormLogin();
		}
	break;

	case 'addPromo':
	if (isset($_SESSION['admin']) && $_SESSION['admin']===true){
		require_once ('../Model/class/promo.php');
    require_once ('../Model/class/formateur.php');
    require_once ('../Model/class/formateurPromo.php');
		$promo= new Promo(
			null,
			$_POST['dateDeb'],
			$_POST['dateFin'],
			$_POST['lieu'],
			$_POST['loginInter'],
			$_POST['heureTypeDeb'],
			$_POST['heureTypeFin']
		);
		$promo->save();
    if (isset($_POST['nom'])&& $_POST['nom']!== null ){
      for($i=0;$i<count($_POST['nom']);$i++){
        $formateur=new Formateur(
          null,
          $_POST['nom'][$i],
          $_POST['prenom'][$i],
          $_POST['login'][$i]
        );
        $formateur->save();
        $formateurPromo= new FormateurPromo(
          null,
          $formateur->getId(),
          $promo->getId()
        );
        $formateurPromo->save();
      }
      $liste = Promo::getList();
      $viewAdmin->displayList($liste);
    }
	}
	else {
		$viewAdmin->displayFormLogin();
	}
  // case 'modifier':$viewAdmin->displayFormPromo();
  // break;

  // case 'supprimer':$viewAdmin->displayFormPromo();
  // break;

}
?>
</body>
</html>